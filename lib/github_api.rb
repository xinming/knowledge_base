# require 'rubygems'
require 'open-uri'
require 'json'
module GitHub
  BASE_URL = "https://api.github.com/repos/"
  
  class APIRequestError < Exception
  end
  
  
  class Repository
    attr_accessor :name, :language, :git_url, :homepage, :description, :raw
    def initialize(repo_url)
      request_url = BASE_URL + repo_url.gsub("https://github.com/", "")
      response = open(request_url)
      response_code = response.status
      raise(APIRequestError, response_code[1]) if(response_code[0] != "200")
      
      
      response_data = JSON.parse(response.read)
      
      [:name, :language, :git_url, :homepage, :description].each do |attr|
        self.send((attr.to_s + "=").to_sym, response_data[attr.to_s])
      end

      self.raw = response
    end
  end
  
  

  
end


# puts GitHub::Repository.new("https://github.com/joelmoss/strano").inspect