$:.unshift(File.expand_path('./lib', ENV['rvm_path']))
require "rvm/capistrano"
require "bundler/capistrano"
require "capistrano_colors"


#Info
set :rails_env, ENV['rails_env'] || ENV['RAILS_ENV'] || "production"
set :ruby_version, "1.9.2-p290"
set :application_name, "knowledge_base"
set :application, "#{application_name}_#{rails_env}"
set :repository,  "git@bitbucket.org:xinming/knowledge_base.git"

##Rvm
set :rvm_ruby_string, "#{ruby_version}@#{application_name}"
set :rvm_type, :user

#Remote User&Pass
set :user, "dgm59"
set :password, "-&'@$_Q4D6v>B7V=3]Xpw"

#Git User&Pass
set :scm, "git"
set :scm_username, "VbaT"
set :scm_passphrase, "159515"
set :branch, "master"

#deploy
set :deploy_to, "/home/dgm59/git/#{application}"
set :deploy_via, :remote_cache
set :use_sudo, false
set :server_ip, "106.187.46.159"
set :ssh_hostname, "knowledge.dev.thisiscolony.com"
set :keep_releases, 2

#Shared
set :shared_assets, "#{deploy_to}/public"


role :web, server_ip                        # Your HTTP server, Apache/etc
role :app, server_ip                        # This may be the same as your `Web` server
role :db,  server_ip, :primary => true      # This is where Rails migrations will run

after 'deploy:setup', 'deploy:create_shared_folder'
after 'deploy:update_code', 'deploy:symlink_shared_folder'
after 'deploy:symlink_shared_folder', 'deploy:cleanup'

namespace :deploy do
  desc "Create shared folders when setup"
  task :create_shared_folder do
    run "mkdir -p #{shared_path}/public/assets"
    run "mkdir -p #{shared_path}/db"
    run "touch #{shared_path}/db/#{rails_env}.sqlite3"
  end
  desc "Symlink shared configs and folders on each release."
  task :symlink_shared_folder do
      run "rm -rf #{release_path}/public/assets && ln -nfs #{shared_path}/public/assets #{release_path}/public/assets"
      run "rm #{release_path}/db/#{rails_env}.sqlite3 && ln -nfs #{shared_path}/db/#{rails_env}.sqlite3 #{release_path}/db/#{rails_env}.sqlite3"
  end
end

namespace :unicorn do
  desc "Start Unicorn"
  task :start do
    run "cd #{current_path} && bundle exec unicorn_rails -c config/unicorn.rb -E #{rails_env} -D"
  end
  desc "Stop Unicorn"
  task :stop do
    run "cd #{current_path} && kill -9 `cat tmp/pids/unicorn.pid`"
  end
  desc "Restart Unicorn"
  task :restart do
    run "cd #{current_path} && kill -s USR2 `cat tmp/pids/unicorn.pid`"
  end
  desc "Get Unicorn PID"
  task :pid do
    run "cd #{current_path} && cat tmp/pids/unicorn.pid"
  end
  desc "Read Rails Log"
  task :log do
    run "cd #{current_path} && tail -f log/#{rails_env}.log" do |channel, stream, data|
      puts  # for an extra line break before the host name
      puts "#{channel[:host]}: #{data}" 
      break if stream == :err    
    end
  end
end

