class PagesController < InheritedResources::Base
  # autocomplete :tag, :name, :class_name => 'ActsAsTaggableOn::Tag'
  
  autocomplete :tag, :name, :class_name => 'ActsAsTaggableOn::Tag'


  def create
    @page = Page.new(params[:page])
    if @page.save
      redirect_to "/editor/pages/#{@page.id}/raw"
    else
      redirect_to root_path, :flash => {:error => @page.errors}
    end
  end
  
  
  def destroy
    destroy!{
      redirect_to root_path
      return
    }
  end
  
  
  def mercury_update
    page = Page.find(params[:id])
    page.body = params[:content][:page_body][:value]
    page.save!
    # Update page
    render text: ""
  end
  
  
  def raw
    page = Page.find(params[:id])
    @is_raw = true
    render :show
  end
end
