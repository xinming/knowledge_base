class Category < ActiveRecord::Base
  has_many :links
  
  
  def to_s
    name
  end
end
