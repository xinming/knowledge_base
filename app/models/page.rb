class Page < ActiveRecord::Base
  acts_as_taggable
  validates :name, :uniqueness => true, :presence => true
  validates :description, :presence => true
  validates :project, :presence => true
  belongs_to :user
  belongs_to :project
  
  def primary_tag
    project.name
  end
end
