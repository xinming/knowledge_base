require 'open-uri'

class Link < ActiveRecord::Base

  acts_as_taggable
  belongs_to :user
  belongs_to :category
  validates :url, :uniqueness => true, :presence => true
  validates :name, :presence => true, :presence => true
  validates :content, :presence => true
  validates :category, :presence => true
  before_validation :fetch_info
  
  
  def fetch_info
    unless(self.url.blank?)
      source = open(self.url.strip).read
      readable = Readability::Document.new(source, :tags => %w[div p img a ul li h4 h3 h2 h1],
      :attributes => %w[src href], 
      :remove_empty_nodes => false)
      self.name = readable.title.strip
      self.content = readable.content.strip
      
      begin; self.keywords = readable.html.search("meta[name=keywords]").first["content"].split(/[ \,]+/); rescue; end
      begin; self.description += readable.html.search("meta[name=description]").first["content"]; rescue; end
      self.tag_list += self.keywords if self.keywords
    end
  end
  
  def language
    nil
  end
  
  
  def primary_tag
    self.category.name
  end

end
