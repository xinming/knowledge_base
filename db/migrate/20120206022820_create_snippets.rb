class CreateSnippets < ActiveRecord::Migration
  def change
    create_table :snippets do |t|
      t.string :name
      t.text :embed_code
      t.text :body
      t.integer :language_id
      t.text :description
      t.integer :user_id
      t.timestamps
    end
  end
end
