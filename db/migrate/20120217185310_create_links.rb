class CreateLinks < ActiveRecord::Migration
  def change
    create_table :links do |t|
      t.string :name
      t.string :description
      t.text :content
      t.string :url
      t.integer :category_id
      t.text :keywords
      t.integer :user_id
      t.timestamps
    end
  end
end
